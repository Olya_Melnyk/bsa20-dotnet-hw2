﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; set; }
        public string VehicleId { get; private set; }
        public DateTime Time { get; private set; }
        public TransactionInfo(string vehicleId, decimal sum)
        {
            Sum = sum;
            VehicleId = vehicleId;
            Time = DateTime.Now;
        }
    }
}
