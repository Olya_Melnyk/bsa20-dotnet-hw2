﻿using System;
using System.Text.RegularExpressions;
using CoolParking.BL.Models;
namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; set; }
        public Vehicle(string id,VehicleType type, decimal balance)
        {
                Id = Validation(id) ? id : throw new ArgumentException("Не коректний ідентифікатор транспортного засобу");
                VehicleType = type;
                Balance = balance>=0?balance:throw new ArgumentException("Баланс не може бути від'ємним при додаванні транспортного засобу");
        }
        public Vehicle(VehicleType type, decimal balance)
        {
            Id = GenerateRandomRegistrationPlateNumber();
            VehicleType = type;
            Balance = balance >= 0 ? balance : throw new ArgumentException("Баланс не може бути від'ємним при додаванні транспортного засобу");
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random rdn = new Random();
            string st = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return (st[rdn.Next(0, st.Length - 1)]).ToString() + (st[rdn.Next(0, st.Length - 1)]).ToString()
                + "-" + rdn.Next(1000, 9999) + "-" + (st[rdn.Next(0, st.Length - 1)]).ToString() + (st[rdn.Next(0, st.Length - 1)]).ToString();
        }
        public static bool Validation(string id)
        {
            Regex regex = new Regex(@"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
            return regex.IsMatch(id);
        }
        public override string ToString()
        {
            return "ID:" + Id.ToString() + ";\tТип транспортного засобу:" + VehicleType.ToString() + ";\tБаланс:" + Balance.ToString();
        }
    }
}