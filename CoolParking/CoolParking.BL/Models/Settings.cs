﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System;
namespace CoolParking.BL.Models
{
    public static class Settings
    {
         static public decimal StartBalance { get; private set; } = 0;
        static public int ParkingSize { get; private set; } = 10;
        static public int PayTime { get; private set; } = 5000;
        static public decimal CarPay { get; private set; } = 2;
        static public decimal BusPay { get; private set; } = 3.5M;
        static public decimal TruckPay { get; private set; } = 5;
        static public decimal MotorcyclePay { get; private set; } = 1;
        static public decimal Fine { get; private set; } = 2.5m;
        static public int WritePeriod { get; private set; } = 60000;

    }
}
