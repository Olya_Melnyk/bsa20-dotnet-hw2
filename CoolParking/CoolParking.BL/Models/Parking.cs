﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
namespace CoolParking.BL.Models
{

    public class Parking
    {
        private static Parking instance;
        internal decimal Balance { get; set; } = Settings.StartBalance;
        internal List<Vehicle> vehicleList { get; set; } = new List<Vehicle>(Settings.ParkingSize);
        public List<TransactionInfo> TransactionList { get; set; } = new List<TransactionInfo>();
        protected Parking() {}
        public static Parking GetInstance()
        {
            if (instance == null)
            {
                return instance = new Parking();
            }
            else return instance;
        }
    }
}
