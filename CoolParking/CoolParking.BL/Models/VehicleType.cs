﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.
using System;

namespace CoolParking.BL.Models
{
   public enum VehicleType
    {
        Bus=0,
        PassengerCar=1,
        Truck=2,
        Motorcycle=4
    }

}
