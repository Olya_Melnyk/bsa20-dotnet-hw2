﻿using System;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using CoolParking.BL;
using System.Threading;
using System.ComponentModel;
using System.Linq;

namespace ConsoleMenu
{
    class Program
    {
        static void Main()
        {
            Parking parking = Parking.GetInstance();
            Thread thread = new Thread(new ThreadStart(Working));
            thread.Start();
            Console.WriteLine("Привiт! Вiтаю тебе на CoolParking!");
            bool flag = true;
            int answ = 0;
            while (flag == true)
            {
                ChooseAction();
                Console.WriteLine("Чи хочеш ти виконати ще якусь дiю? 1-так/2-нi");
                answ = int.Parse(Console.ReadLine());
                if (answ != 1)
                flag = false;          
            }
            Console.WriteLine("На все добре!");
         
        }
        static ITimerService timerPay = new TimerService();
        static ITimerService timerLog = new TimerService();
        static ILogService logService = new LogService("log.txt");
        static ParkingService parkingService = new ParkingService(timerPay, timerLog, logService);
        public static void ChooseAction()
        {
            int action = 0;
            bool flag = false;
            Console.WriteLine("Обери, що ти хочеш зробити");
            Console.WriteLine("1-Побачити поточний баланс CoolParking \n 2-Побачити суму зароблених коштiв за поточний перiод\n" +
             "3-Побачити кiлькість вiльних мiсць\n4-Побачити всю iсторiю Транзакцiй\n5-Побачити список транспортих засобiв, що зараз знаходяться на паркiнгу" +
             "\n5-Додати новий транспортний засiб\n6-Забрати транспортний засiб з паркiнгу\n7-Поповнити баланс конкретного транспортного засобу\n" +
             "8-Побачити Транзакцiї за поточний перiод");
            Console.WriteLine("Введи свою відповiдь: ");
            try
            {
                action = int.Parse(Console.ReadLine());
                flag = true;
            }
            catch (Exception) { Console.WriteLine("Введи коректне значення"); action = int.Parse(Console.ReadLine()); }
            
            while (flag==true)
            {
                switch (action)
                {
                    case 1:
                        GetBalanceParking();
                        flag = false;
                        break;
                    case 2:
                        SumCurrentTransaction();
                        flag = false;
                        break;
                    case 3:
                        FreePlaces();
                        flag = false;
                        break;
                    case 4:
                        AllTransactions();
                        flag = false;
                        break;
                    case 5:
                        AllVehicleOnParking();
                        flag = false;
                        break;
                    case 6:
                        RemoveVehicleWithParking();
                        flag = false;
                        break;
                    case 7:
                        TopUpBalanceVehicle();
                        flag = false;
                        break;
                    case 8:
                        AllTransactionsForLastPeriod();
                        flag = false;
                        break;
                    case 9:
                        AddVehicleOnParking();
                        flag = false;
                        break;
                    default:
                        Console.WriteLine("Введи один з варіантів");
                        break;
                }
            }
        }
        public static void Working()
        {
            timerPay.Start();
            timerLog.Start();
        }
        public static void GetBalanceParking()
        {
            Console.WriteLine($"Поточний баланс CoolParking: {parkingService.GetBalance()}");
        }
        public static void SumCurrentTransaction()
        {
            decimal sum = 0;
            for (int i = 0; i <= parkingService.GetLastParkingTransactions().Length - 1; i++)
            {
                decimal sumCurrentTrans = parkingService.GetLastParkingTransactions()[i].Sum;
                sum += sumCurrentTrans;
            }
            Console.WriteLine($"Сума поточних транзакцiй до запису в журнал {sum}");
        }
        public static void FreePlaces()
        {
            Console.WriteLine($"Кiлькiсть вiльних мiсць на парковцi {parkingService.GetFreePlaces()} з {Settings.ParkingSize}");
        }
        public static void AllTransactionsForLastPeriod()
        {
            Console.WriteLine("Список транзакцiй за останнiй перiод:");
            string write = "";
            foreach (var item in parkingService.GetLastParkingTransactions())
            {
                write += $"Час транзпкцiї: {item.Time}; Сума трпнзакцiї:\t{item.Sum};\tIдентифiкатор транспортного засобу{item.VehicleId}\n";
            }
            Console.Write(write);

        }
        public static void AllTransactions()
        {
            Console.WriteLine("Список всiх транзакцiй парковки:");
            Console.WriteLine(parkingService.ReadFromLog());
        }
        public static void AllVehicleOnParking()
        {
            Console.WriteLine("Список всiх транспортних засобiв на парковцi:");
            string write="";
            foreach (var item in parkingService.GetVehicles().ToArray())
            {
                write =write + item.ToString() + "\n";
            }
            Console.Write(write);
        }
        public static void AddVehicleOnParking()
        {
            VehicleType type=VehicleType.Bus;
            decimal balance = 0;
            string ID;
            bool repeat = false;
            int input;
            Vehicle vehicle = null;
            Console.WriteLine("Ти обрав додати транспортний засiб на парковку!");
            Thread.Sleep(5);
            Console.WriteLine("Обери тип транспортного засобу");
            Console.WriteLine("1-Автобус, 2-Легковий автомобiль,\n 3-Грузовий автомобiль, 4-Мотоцикл");
            input = int.Parse(Console.ReadLine());
            while (repeat==false)
            {
                switch (input)
                {
                    case 1:
                        type= VehicleType.Bus;
                        repeat = true;
                        break;
                    case 2:
                        type = VehicleType.PassengerCar;
                        repeat = true;
                        break;
                    case 3:
                        type = VehicleType.Truck;
                        repeat = true;
                        break;
                    case 4:
                        type = VehicleType.Motorcycle;
                        repeat = true;
                        break;
                    default:
                        Console.WriteLine("Введи коректне значення");
                        break;
                }
            }
            Console.WriteLine("Додай суму для свого балансу");
            try
            {
                balance = decimal.Parse(Console.ReadLine());
            }
            catch (Exception) { new Exception("Введи коректне значення"); }
            Console.WriteLine("Додати iдентифiкатор транспортного засобу можна двома способами:\n1-Введу самостiйно, 2-Згенерувати автоматично");
            input = int.Parse(Console.ReadLine());
            repeat = true;
            while (repeat==true)
            {
                switch (input)
                {
                    case 1:
                        Console.WriteLine("Окей, тепер введи iдентифiктор в такому форматi: XX-YYYY-XX, де Х-будь-яка латинська буква у верхньому регiстрi, Y-будь-яка цифра");
                        ID = Console.ReadLine();
                        vehicle = new Vehicle(ID, type, balance);
                        repeat = false;
                        break;
                    case 2:
                        vehicle = new Vehicle(type, balance);
                        Console.WriteLine("Окей, твiй ідентифiкатор: {0}", vehicle.Id);
                        repeat = false;
                        break;
                    default:
                        Console.WriteLine("Обери коректний варіант");
                        break;

                }
            }
            parkingService.AddVehicle(vehicle);
        }
        public static void RemoveVehicleWithParking()
        {
            Console.WriteLine("Введи iдентифiкатор транспортного засобу");
            string id = Console.ReadLine();
            bool flag = true;
            while (flag)
            {
                if (Vehicle.Validation(id))
                {
                    parkingService.RemoveVehicle(id);
                    flag = false;
                }
                else Console.WriteLine("Не коректний iдентифікатор. Введи ще раз");
            }
            Console.WriteLine("Ти успішно видалив транспортний засiб!");
             
        }
        public static void TopUpBalanceVehicle()
        {
            Console.WriteLine("Для того щоб поповнити транспортний засіб введіть його ідентифікатор");
            string id = Console.ReadLine();
            bool flag = true;
            decimal sum = 0;
            Console.WriteLine("Введіть сума, на яку хочете поповнити транспортний засіб");      
            sum = decimal.Parse(Console.ReadLine());           
            while (flag)
            {
                if (Vehicle.Validation(id))
                {
                    parkingService.TopUpVehicle(id,sum);
                    flag = false;
                }
                else Console.WriteLine("Не коректний ідентифікатор. Введіть ще раз");
            }
            Console.WriteLine($"Ви успішно поповнили засіб {id}");
        }
    }
}
